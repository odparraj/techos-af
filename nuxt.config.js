export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Techos AF - Drywall & PVC',
    htmlAttrs: {
      lang: 'es'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap' },
      { href: 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', rel: 'stylesheet' },
      { href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css', rel: 'stylesheet' },
      { href: 'lib/flaticon/font/flaticon.css', rel: 'stylesheet' },
      { href: 'lib/animate/animate.min.css', rel: 'stylesheet' },
      { href: 'lib/owlcarousel/assets/owl.carousel.min.css', rel: 'stylesheet' },
      { href: 'lib/lightbox/css/lightbox.min.css', rel: 'stylesheet' },
      { href: 'lib/slick/slick.css', rel: 'stylesheet' },
      { href: 'lib/slick/slick-theme.css', rel: 'stylesheet' },
      { href: 'css/style.css', rel: 'stylesheet' },
    ],
    script: [
      { src:'https://code.jquery.com/jquery-3.4.1.min.js', type: 'text/javascript', body: true},
      { src:'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js', type: 'text/javascript', body: true},
      { src:'lib/easing/easing.min.js', type: 'text/javascript', body: true},
      { src:'lib/wow/wow.min.js', type: 'text/javascript', body: true},
      { src:'lib/owlcarousel/owl.carousel.min.js', type: 'text/javascript', body: true},
      { src:'lib/isotope/isotope.pkgd.min.js', type: 'text/javascript', body: true},
      { src:'lib/lightbox/js/lightbox.min.js', type: 'text/javascript', body: true},
      { src:'lib/waypoints/waypoints.min.js', type: 'text/javascript', body: true},
      { src:'lib/counterup/counterup.min.js', type: 'text/javascript', body: true},
      { src:'lib/slick/slick.min.js', type: 'text/javascript', body: true},
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  router: {
    base: '/techos-af/'
  },

  generate: {
    dir: 'public'
  },

}
